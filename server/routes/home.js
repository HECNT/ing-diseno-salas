var express        = require('express');
var router         = express.Router();
var ctrl           = require('../controllers/home');
var fs             = require('fs');
var verifyToken    = require('./middleware');
var jwt            = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config         = require('./config');
var json2xls = require('json2xls');
var path = require('path');

router.post('/login', doLogin);
router.post('/set-participante', setParticipante);
router.post('/get-report', getReport);
router.post('/desactivar-sala', desactivarSala);
router.post('/new-sala', setNewSala);
router.post('/get-participante-agenda', getParticipanteAgenda);
router.post('/set-new-agenda', setNewAgenda);
router.post('/get-validar-horario', getValidateHorario);
router.post('/desactivar-agenda', desactivarAgenda);
router.post('/validar-desactivar', validarDesactivar);
router.post('/crear-usuario', crearUsuario);
router.post('/cambiar-pass', cambiarPass);
router.post('/validar-externo', validarExterno);

router.put('/kill', doKill);

router.get('/get-sala', getSala);
router.get('/get-horario', getHorario);
router.get('/get-participante', getParticipante);
router.get('/get-agenda', getAgenda);

function doLogin(req, res) {
  var d = req.body;
  ctrl.doLogin(d)
  .then(function(result){
    if (result.err) {
      res.json(result)
    } else {
      ctrl.verificarPerfil(d)
      .then(function(resu){
        if (resu.err) {
          res.json(resu)
        } else {
          var config = {
              user: d.usuario,
              password: d.pass,
              server: 'localhost',
              database: 'salas_2'
          };
          if (resu.admin) {
            config.normal = false
            req.session.usuario = config
            res.json({err: false});
          } else {
            config.normal = true
            req.session.usuario = config
            res.json({err: false});
          }
        }
      })
    }
  })
}

function doKill(req, res) {
  req.session.usuario = null
  res.json({err: false})
}

function getSala(req, res) {
  var d = req.session.usuario;
  ctrl.getSala(d)
  .then(function(result){
    res.json(result)
  })
}

function getHorario(req, res) {
  var d = req.session.usuario;
  ctrl.getHorario(d)
  .then(function(result){
    res.json(result)
  })
}

function getParticipante(req, res) {
  var d = req.session.usuario;
  ctrl.getParticipante(d)
  .then(function(result){
    res.json(result)
  })
}

function setParticipante(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.setParticipante(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function getReport(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.getReport(d, datos)
  .then(function(result){
    var xls = json2xls(result.data.recordset);
    fs.writeFileSync(path.join(__dirname , '..', '..', 'public', 'reporte', 'report.xlsx'), xls, 'binary');
    res.json(result)
  })
}

function desactivarSala(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.desactivarSala(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function setNewSala(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.setNewSala(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function getAgenda(req, res) {
  var d = req.session.usuario;
  ctrl.getAgenda(d)
  .then(function(result){
    res.json(result)
  })
}

function getParticipanteAgenda(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.getParticipanteAgenda(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function setNewAgenda(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.setNewAgenda(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function getValidateHorario(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.getValidateHorario(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function desactivarAgenda(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.desactivarAgenda(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function validarDesactivar(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.validarDesactivar(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function crearUsuario(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.crearUsuario(d, datos)
  .then(function(result){
    res.json(result)
  })
}

function cambiarPass(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.cambiarPass(d, datos)
  .then(function(result){
    if (result.err) {
      res.json(result)
    } else {
      req.session.usuario = null
      res.json(result)
    }
  })
}

function validarExterno(req, res) {
  var datos = req.body;
  var d = req.session.usuario;
  ctrl.validarExterno(d, datos)
  .then(function(result){
    res.json(result)
  })
}

module.exports = router;
