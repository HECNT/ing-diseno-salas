var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  if (req.session.usuario) {
    res.redirect('inicio')
  } else {
    res.redirect('login')
  }
});

router.get('/inicio', function(req, res) {
  if (req.session.usuario) {
    res.render('inicio', { usuario: true, url: `https://localhost:${PUERTO}/`, normal : req.session.usuario.normal })
  } else {
    res.redirect('login');
  }
});

router.get('/agenda', function(req, res) {
  if (req.session.usuario) {
    res.render('agenda', { usuario: true, url: `https://localhost:${PUERTO}/`, normal : req.session.usuario.normal })
  } else {
    res.redirect('login');
  }
});

router.get('/about', function(req, res) {
  if (req.session.usuario) {
    res.render('about', { usuario: true, url: `https://localhost:${PUERTO}/`, normal : req.session.usuario.normal })
  } else {
    res.redirect('login');
  }
});

router.get('/crear', function(req, res) {
  if (req.session.usuario) {
    res.render('crear', { usuario: true, url: `https://localhost:${PUERTO}/`, normal : req.session.usuario.normal })
  } else {
    res.redirect('login');
  }
});

router.get('/perfil', function(req, res) {
  if (req.session.usuario) {
    res.render('perfil', { usuario: true, url: `https://localhost:${PUERTO}/`, normal : req.session.usuario.normal })
  } else {
    res.redirect('login');
  }
});

router.get('/login', function(req, res) {
  if (req.session.usuario) {
    res.redirect('inicio');
  } else {
    res.render('login', { usuario: false, url: `https://localhost:${PUERTO}/` })
  }
});

module.exports = router;
