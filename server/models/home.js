var sql = require("mssql");

module.exports.doLogin = function (d) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.usuario,
         password: d.pass,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `SELECT 1+1 AS res`
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getSala = function (d) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `SELECT * FROM sala WHERE activo = 1`
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getHorario = function (d) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `SELECT * FROM horario WHERE activo = 1`
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getParticipante = function (d) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           select
            	(nombre + ' ' + apellido_paterno + ' ' + apellido_materno) as nombre_completo,
              participante_id
            from
            	participante
            where
              disponible = 1
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.setParticipante = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           INSERT INTO
            participante (nombre, apellido_paterno, apellido_materno, edad, email, telefono)
           VALUES
            ('${datos.nombre}', '${datos.ap}', '${datos.am}', '${datos.edad}', '${datos.correo}', '${datos.telefono}')

           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getId = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           SELECT
            	MAX(participante_id) as participante_id
            FROM
            	participante
           `
           request.query(query, function (err, recordset) {
             console.log(recordset,'**********recordset');
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, participante_id: recordset.recordset[0].participante_id})
               }
           });
         }
     });
  })
}

module.exports.getReport = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           SELECT
        		a2.nombre as sala,
        		a1.asunto,
        		a1.fecha,
        		a1.cafeteria,
        		a1.proyector,
        		a1.pizarron
        	FROM
        		agenda as a1
        	INNER JOIN
        		sala as a2
        	ON
        		a1.sala_id = a2.sala_id
          WHERE
            a1.sala_id = ${datos.sala_id}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.desactivarSala = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           UPDATE
              sala
            SET
              activo = 0
            WHERE
              sala_id = ${datos.sala_id}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.setNewSala = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           INSERT INTO
              sala
            VALUES
              ('${datos.nombre}', 1)
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getAgenda = function (d) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           select
        		a1.agenda_id,
        		a1.fecha,
        		a1.asunto,
        		a1.cafeteria,
        		a1.proyector,
        		a1.pizarron,
        		(a2.ini + '-' + a2.fin) as horario
        	from
        		agenda as a1
        	inner join
        		horario as a2
        	on
        		a1.horario_id = a2.horario_id
        	where
        		a1.activo = 1
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getParticipanteAgenda = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `

          SELECT
           a2.agenda_id,
           a2.fecha as fecha_agenda,
           a2.asunto,
           a2.cafeteria,
           a2.proyector,
           a2.pizarron,
           (a3.nombre + ' ' + a3.apellido_paterno + ' ' + a3.apellido_materno) as nombre_completo,
           a4.nombre as sala
          FROM
           agenda_participante as a1
          INNER JOIN
           agenda as a2
          ON
           a1.agenda_id = a2.agenda_id
          INNER JOIN
           participante as a3
          ON
           a1.participante_id = a3.participante_id
          INNER JOIN
           sala as a4
          ON
           a2.sala_id = a4.sala_id
          WHERE
           a2.agenda_id = ${datos.agenda_id}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.setNewAgenda = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `

           INSERT INTO
           	agenda (fecha_captura, fecha, asunto, cafeteria, proyector, pizarron, activo, sala_id, horario_id, usuario_id)
           VALUES
           	(getDate(), '${datos.fecha}', '${datos.asunto}', '${datos.cafeteria}', '${datos.proyector}', '${datos.pizarron}', 1, '${datos.sala_id}', '${datos.horario_id}', 2)
           `
           console.log(query,'QUERY AGENDA Agenda');
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.setAgendaParticipante = function (d, datos, agenda_id) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
              INSERT INTO
                agenda_participante (participante_id, agenda_id)
              VALUES
                (${datos}, ${agenda_id})
           `
           console.log(query,'QUERY AGENDA PARTICIPANTE');
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.getIdAgenda = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           SELECT
            	MAX(agenda_id) as agenda_id
            FROM
            	agenda
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, agenda_id: recordset.recordset[0].agenda_id})
               }
           });
         }
     });
  })
}

module.exports.getValidateHorario = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           select
            	agenda_id, sala_id, horario_id
            from
            	agenda
            where
              sala_id = ${datos.sala_id}
            and
              activo = 1
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.desactivarAgenda = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           UPDATE
              agenda
            SET
              activo = 0
            WHERE
              agenda_id = ${datos.agenda_id}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, data: recordset})
               }
           });
         }
     });
  })
}

module.exports.validarDesactivar = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           select
            	a1.agenda_id,
            	a2.sala_id
            from
            	agenda as a1
            inner join
            	sala as a2
            on
            	a1.sala_id = a2.sala_id
            where
              a2.sala_id = ${datos.sala_id}
            and
              a1.activo = 1
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, si: recordset.recordset.length > 0 ? true : false})
               }
           });
         }
     });
  })
}

module.exports.updateParticipante = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
           UPDATE
              participante
            SET
              disponible = 0
            WHERE
              participante_id = ${datos}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.getParticipanteUpdate = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            SELECT
              *
            FROM
              agenda_participante
            WHERE
              agenda_id = ${datos.agenda_id}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, participante: recordset.recordset})
               }
           });
         }
     });
  })
}

module.exports.updateParticipanteDesactivar = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            UPDATE
              participante
            SET
              disponible = 1
            WHERE
              participante_id = ${datos}
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.createLogin = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            CREATE LOGIN ${datos.usuario} WITH PASSWORD = 'HaloW117'
           `
           console.log(query,'LOGIN');
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.crearUsuario = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            CREATE USER ${datos.usuario} FOR LOGIN ${datos.usuario}
           `
           console.log(query,'USUARIO');
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.grantUser = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            GRANT SELECT, INSERT, UPDATE TO ${datos.usuario}
           `
           console.log(query,'GRANT');
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.crearUsuarioSistema = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            INSERT INTO
              usuario
            VALUES
              ('${datos.usuario}', '${datos.usuario}', 1, ${datos.perfil})
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.verificarPerfil = function (d) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.usuario,
         password: d.pass,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            SELECT
              *
            FROM
              usuario
            WHERE
              usuario_login = '${d.usuario}'
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false, admin: recordset.recordset[0].perfil_id === 1 ? true : false})
               }
           });
         }
     });
  })
}

module.exports.cambiarPass = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            ALTER LOGIN ${d.user} WITH PASSWORD = '${datos.nueva}' OLD_PASSWORD = '${d.password}'
           `
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: false})
               }
           });
         }
     });
  })
}

module.exports.validarExterno = function (d, datos) {
  return new Promise(function(resolve, reject) {
    var config = {
         user: d.user,
         password: d.password,
         server: 'localhost',
         database: 'salas_2'
     };
     sql.close()
     sql.connect(config, function (err) {
         if (err) {
           resolve({err: true, description: err})
         } else {
           var request = new sql.Request();
           var query = `
            SELECT
              *
            FROM
              participante
            WHERE
              nombre = '${datos.nombre}'
            AND
              apellido_paterno = '${datos.ap}'
            AND
              apellido_materno = '${datos.am}'
           `
           console.log(query);
           request.query(query, function (err, recordset) {
               if (err) {
                 resolve({err: true, description: err})
               } else {
                 resolve({err: recordset.recordset.length > 0 ? true : false})
               }
           });
         }
     });
  })
}
