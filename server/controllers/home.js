var model = require('../models/home')
var Promise = require('bluebird')

module.exports.doLogin = function (d) {
  return new Promise( async function(resolve, reject) {
    if ( d.usuario.length === 0 || d.pass.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.doLogin(d)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.getSala = function (d) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getSala(d)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.getHorario = function (d) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getHorario(d)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.getParticipante = function (d) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getParticipante(d)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.setParticipante = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.setParticipante(d, datos)
      .then(function(res){
        model.getId(d)
        .then(function(result){
          resolve(result);
        })
      })
    }
  })
}

module.exports.getReport = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getReport(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.desactivarSala = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.desactivarSala(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.setNewSala = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.setNewSala(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.getAgenda = function (d) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getAgenda(d)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.getParticipanteAgenda = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getParticipanteAgenda(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.setNewAgenda = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.setNewAgenda(d, datos)
      .then(function(res){
        model.getIdAgenda(d)
        .then(function(res){
          Promise.each(datos.participantes,function(arr){
            return new Promise(function(resolve, reject) {
              model.setAgendaParticipante(d, arr, res.agenda_id)
              .then(function(res){
                model.updateParticipante(d, arr)
                .then(function(res){
                  resolve()
                })
              })
            })
          }).then(function(){
            resolve({err: false})
          })
        })
      })
    }
  })
}

module.exports.getValidateHorario = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.getValidateHorario(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.desactivarAgenda = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.desactivarAgenda(d, datos)
      .then(function(res){
        model.getParticipanteUpdate(d, datos)
        .then(function(res){
          var id = []
          for(var key in res.participante) {
            id.push(res.participante[key].participante_id)
          }
          Promise.each(id, function(participante_id){
            return new Promise(function(resolve, reject) {
              model.updateParticipanteDesactivar(d, participante_id)
              .then(function(res){
                resolve()
              })
            });
          }).then(function(){
            resolve(res);
          })
        })
      })
    }
  })
}

module.exports.validarDesactivar = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.validarDesactivar(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.crearUsuario = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.createLogin(d, datos)
      .then(function(res){
        if (res.err) {
          resolve(res)
        } else {
          model.crearUsuario(d, datos)
          .then(function(res){
            if (res.err) {
              resolve(res)
            } else {
              model.grantUser(d, datos)
              .then(function(res){
                model.crearUsuarioSistema(d, datos)
                .then(function(res){
                  resolve(res)
                })
              })
            }
          })
        }
      })
    }
  })
}

module.exports.verificarPerfil = function (d) {
  return new Promise( async function(resolve, reject) {
    model.verificarPerfil(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.cambiarPass = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.cambiarPass(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}

module.exports.validarExterno = function (d, datos) {
  return new Promise( async function(resolve, reject) {
    if ( d.user.length === 0 || d.password.length === 0 ) {
      resolve({err: true, description: "JSON invalido"})
    } else {
      model.validarExterno(d, datos)
      .then(function(res){
        resolve(res);
      })
    }
  })
}
