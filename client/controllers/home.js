require('../services/home');
var swal = require('sweetalert');
var moment = require('moment');

angular.module(MODULE_NAME)
.controller('homeCtrl', ['$scope', 'HomeService', '$timeout', 'socket', function($scope, HomeService, $timeout, socket) {

  $scope.logon = { usuario: "", pass: "" };
  $scope.externo = { nombre: "", ap: "", am: "", edad: "", correo: "", telefono: "" }
  $scope.reporte = {}
  $scope.nueva_sala = { nombre: "" }
  $scope.crear_usuario = {usuario: "", perfil: ""}
  $scope.cambiar_user = {actual: "", nueva: ""}
  $scope.nueva = {asunto: "", fecha:"", pizarron:false, proyector:false, cafeteria:false}
  $scope.participantes = [];

  $scope.btnDoLogin = btnDoLogin;
  $scope.btnCierra = btnCierra;
  $scope.inicio = inicio;
  $scope.btnShowAgenda = btnShowAgenda;
  $scope.btnSeleccionarHorario = btnSeleccionarHorario;
  $scope.btnParticipanteExterno = btnParticipanteExterno;
  $scope.btnParticipanteInterno = btnParticipanteInterno;
  $scope.btnAgregarArr = btnAgregarArr;
  $scope.btnGetReporte = btnGetReporte;
  $scope.btnDesactivarSala = btnDesactivarSala;
  $scope.btnAgregarSala = btnAgregarSala;
  $scope.btnEliminar = btnEliminar;
  $scope.getAgenda = getAgenda;
  $scope.btnGetParticipantes = btnGetParticipantes;
  $scope.btnDesactivarAgenda = btnDesactivarAgenda;
  $scope.btnNuevaAgenda = btnNuevaAgenda;
  $scope.cPizarron = cPizarron;
  $scope.cCafeteria = cCafeteria;
  $scope.cProyector = cProyector;
  $scope.changeSala = changeSala;
  $scope.btnCrearUsuario = btnCrearUsuario;
  $scope.btnCambiarPass = btnCambiarPass;
  $scope.changeName = changeName;
  $scope.btnFecha = btnFecha;

  $scope.mi_agenda = true;
  $scope.mi_agenda_form = false;
  $scope.participante_interno = true;
  $scope.participante_externo = false;
  $scope.reporte_msg = true;
  $scope.mi_agenda_cancelar = false;
  $scope.agenda_err = false

  function getAgenda() {
    HomeService.getAgenda()
    .success(function(res){
      if (res.err) {
        alert('Hubo un error')
      } else {
        $scope.list = res.data.recordset;
      }
    })
  }

  function btnFecha() {
    var d = $scope.nueva;
    var date = moment(d.fecha).format('DD/MM/YYYY');
    console.log(isFutureDate(date));
    if (!isFutureDate(date)) {
      swal({
        icon: "error",
        title: "Tienes un error",
        text: "Debes de seleccionar una fecha mayor a la actual"
      })
    }
  }

  function inicio() {
    var content = [];
    $('.ui.checkbox').checkbox();
    HomeService.getSala()
    .success(function(res){
      if (res.err) {
        swal({
          title: "Tienes un error",
          icon: "error",
          text: "Hubo un error al traer la información"
        })
      } else {
        $scope.sala = res.data.recordset
        HomeService.getHorario()
        .success(function(res){
          if (res.err) {
            alert('Tienes un error');
          } else {
            $scope.listHorario = res.data.recordset;
            HomeService.getParticipante()
            .success(function(res){
              console.log(res,'PARTICIPANTE');
              if (res.err) {
                alert('Tienes un error');
              } else {
                var data = res.data.recordset;
                $scope.listParticipante = res.data.recordset;
                for (var key in data) {
                  content.push({ title: data[key].nombre_completo + '-' + data[key].participante_id })
                }
                $('.ui.search').search({ source: content });
              }
            })
          }
        })
      }
    })
  }

  function btnDoLogin() {
    var d = $scope.logon;
    if ( d.usuario.length === 0 || d.pass.length === 0 ) {
      swal({
        title: "Tienes un error",
        icon: "error",
        text: "Todos los campos son necesarios"
      })
    } else {
      HomeService.doLogin(d)
      .success(function(res){
        if (res.err) {
          swal({
            title: "Tienes un error",
            icon: "error",
            text: res.description.originalError.message
          })
        } else {
          location.reload()
        }
      })
    }
  }

  function btnCierra() {
    HomeService.doKill()
    .success(function(res){
      location.reload();
    })
  }

  function btnShowAgenda() {
    var d = { sala_id: $scope.agenda.sala_id }
    $scope.mi_agenda_cancelar = true;
    HomeService.getValidateHorario(d)
    .success(function(res){
      if (res.err) {
        alert("Hubo un error")
      } else {
        for (var key in res.data.recordset) {
          for (var key1 in $scope.listHorario) {
            if (res.data.recordset[key].horario_id*1 === $scope.listHorario[key1].horario_id*1) {
              $scope.listHorario[key1].en_uso = true;
            }
          }
        }
        console.log($scope.listHorario,'HORARIO');
        $scope.mi_agenda = false;
      }
    })
  }

  function changeSala() {
    if ($scope.mi_agenda === false) {
      swal({
        icon: "error",
        title: "Tienes un error",
        text: "Debes de cancelar la sala selecciona para poder continuar"
      })
      $scope.agenda_err = true
    }
  }

  function btnSeleccionarHorario(l) {
    $scope.horario = l;
    $scope.mi_agenda_form = true;
  }

  function btnParticipanteExterno() {
    $scope.participante_externo = true;
    $scope.participante_interno = false;
  }

  function btnParticipanteInterno() {
    $scope.participante_externo = false;
    $scope.participante_interno = true;
  }

  function btnAgregarArr(id) {
    if (id === 1) {
      var d = $scope.externo;
      var d = $scope.externo;
      HomeService.validarExterno(d)
      .success(function(resi){
        if (resi.err) {
          swal({
            icon: "error",
            title: "Tienes un error",
            text: "El participante que quieres agregar ya esta registrado en la base de datos"
          })
        } else {
          if ( d.nombre.length === 0 || d.ap.length === 0 || d.am.length === 0 || d.edad.length === 0 || d.correo.length === 0 ) {
            swal({
              title: "Tienes un error",
              icon: "error",
              text: "Todos los campos con asterisco con necesarios para continuar"
            })
          } else {
            if (isNaN(d.edad)) {
              swal({
                title: "Tienes un error",
                icon: "error",
                text: "El campo edad tiene que ser numero"
              })
            } else {
              if (validateEmail(d.correo)) {
                if (d.edad < 18 || d.edad > 90) {
                  swal({
                    title: "Tienes un error",
                    icon: "error",
                    text: "El campo edad tiene que ser entre 18 y 90"
                  })
                } else {
                  if (isNaN(d.telefono)) {
                    swal({
                      title: "Tienes un error",
                      icon: "error",
                      text: "El campo telefono tiene que ser numero"
                    })
                  } else {
                    if (d.telefono.length < 10 || d.telefono.length > 10) {
                      swal({
                        title: "Tienes un error",
                        icon: "error",
                        text: "El campo telefono tiene que ser de 10 digitos"
                      })
                    } else {
                      HomeService.setParticipante(d)
                      .success(function(res){
                        if (res.err) {
                          alert('Hubo un error')
                        } else {
                          swal({
                            title: "Todo bien",
                            icon: "success",
                            text: "Se agrego al participante en la base de datos"
                          })
                          $scope.participantes.push({nombre: d.nombre + ' ' + d.ap + ' ' + d.am + '-' + res.participante_id})
                          $scope.externo = { nombre: "", ap: "", am: "", edad: "", correo: "", telefono: "" }
                        }
                      })
                    }
                  }
                }
              } else {
                swal({
                  title: "Tienes un error",
                  icon: "error",
                  text: "El correo introducido no es valido"
                })
              }
            }
          }
        }
      })
    } else {
      var nombre = $("#participante-input").val()
      if (nombre.length === 0) {
        swal({
          title: "Tienes un error",
          icon: "error",
          text: "Tienes que seleccionar a un participante"
        })
      } else {
        $scope.participantes.push({nombre: nombre})
        $("#participante-input").val("")
        /*if ($scope.participantes.length === 0) {
          $scope.participantes.push({nombre: nombre})
          $("#participante-input").val("")
        } else {
          for (var key in $scope.participantes) {
            if (nombre === $scope.participantes[key].nombre) {
              swal({
                title: "Tienes un error",
                icon: "error",
                text: "Este participante ya esta agregado"
              })
            } else {
              $("#participante-input").val("")
              $scope.participantes.push({nombre: nombre})
            }
          }
        }*/
      }
    }
  }

  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  function btnGetReporte() {
    var d = $scope.agenda;
    var item = { sala_id: d.sala_id };
    HomeService.getReport(item)
    .success(function(res){
      if (res.err) {
        alert('Hubo un error')
      } else {
        $scope.reporte_msg = false;
      }
    })
  }

  function btnDesactivarSala() {
    var d = $scope.agenda;
    HomeService.validarDesactivar(d)
    .success(function(res){
      if (res.si) {
        swal({
          title: "Tienes un error",
          text: "No se puede desactivar la sala porque hay agendas activas",
          icon: "error"
        })
      } else {
        HomeService.desactivarSala(d)
        .success(function(res){
          if (res.err) {
            alert('Hubo un error')
          } else {
            swal({
              icon: "success",
              title: "Todo bien",
              text: "Se desactivo la sala"
            }).then(function(){
              location.reload()
            })
          }
        })
      }
    })
  }

  function btnAgregarSala() {
    var d = $scope.nueva_sala;
    if (d.nombre.length === 0) {
        swal({
          icon: "error",
          title: "Tienes un error",
          text: "Debes de ingresar un nombre a la sala"
        })
    } else {
      var err = true
      for (var key in $scope.sala) {
        if ($scope.sala[key].nombre.toLowerCase() === d.nombre.toLowerCase()) {
          err = true
        } else {
          err = false
        }
      }
      if (err) {
        swal({
          icon: "error",
          title: "Tienes un error",
          text: "Esta sala ya esta registrada"
        })
      } else {
        HomeService.setNewSala(d)
        .success(function(res){
          if (res.err) {
            alert('Tienes un error')
          } else {
            swal({
              icon: "success",
              title: "Todo bien",
              text: "Se agrego la sala"
            }).then(function(){
              location.reload()
            })
          }
        })
      }
    }
  }

  function btnEliminar(d) {
    var index = $scope.participantes.indexOf(d);
    if (index !== -1) $scope.participantes.splice(index, 1);
  }

  function btnGetParticipantes(l) {
    var d = {agenda_id: l.agenda_id}
    HomeService.getParticipanteAgenda(d)
    .success(function(res) {
      if (res.err) {
        alert('Hubo un error')
      } else {
        $scope.listParticipante = res.data.recordset;
      }
    })
  }

  function btnDesactivarAgenda(l) {
    var d = {agenda_id: l.agenda_id}
    HomeService.desactivarAgenda(d)
    .success(function(res){
      if (res.err) {
        alert('Tienes un error');
      } else {
        swal({
          icon: "success",
          title: "Todo bien",
          text: "Se desactivo la agenda"
        }).then(function(){
          location.reload()
        })
      }
    })
  }

  function cPizarron() {
    $scope.nueva.pizarron = $scope.nueva.pizarron === true ? $scope.nueva.pizarron = false : $scope.nueva.pizarron = true;
  }

  function cProyector() {
    $scope.nueva.proyector = $scope.nueva.proyector === true ? $scope.nueva.proyector = false : $scope.nueva.proyector = true;
  }

  function cCafeteria() {
    $scope.nueva.cafeteria = $scope.nueva.cafeteria === true ? $scope.nueva.cafeteria = false : $scope.nueva.cafeteria = true;
  }

  function btnNuevaAgenda() {
    var d = $scope.nueva;
    d.sala_id = $scope.agenda.sala_id;
    d.participantes = $scope.participantes;
    d.horario_id = $scope.horario.horario_id;
    if (d.asunto.length === 0 || d.fecha.length === 0) {
      swal({
        icon: "error",
        title: "Tienes un error",
        text: "Los campos con asterisco son requeridos"
      })
    } else {
      if (d.participantes.length < 3) {
        swal({
          icon: "error",
          title: "Tienes un error",
          text: "Debes de haber por lo menos 3 participantes"
        })
      } else {
        var participantes = []
        for (var key in d.participantes) {
          var arr = d.participantes[key].nombre.split("-")
          participantes.push(arr[1]*1)
        }
        var uniq = [ ...new Set(participantes) ];
        if (uniq.length < 3) {
          swal({
            icon: "error",
            title: "Tienes un error",
            text: "Debes de haber por lo menos 3 participantes, asegurate de no haber repetidos"
          })
        } else {
          var date = moment(d.fecha).format('DD/MM/YYYY');
          console.log(isFutureDate(date));
          if (isFutureDate(date)) {
            d.participantes = uniq;
            HomeService.setNewAgenda(d)
            .success(function(res){
              if (res.err) {
                alert('Hubo un error');
              } else {
                swal({
                  icon: "success",
                  title: "Todo bien",
                  text: "Se realizo tu agenda, puedes ver los datos de tu agenda en el apartado de Ver agenda"
                }).then(function(){
                  location.href = "/agenda"
                })
              }
            })
          } else {
            swal({
              icon: "error",
              title: "Tienes un error",
              text: "Debes de seleccionar una fecha mayor a la actual"
            })
          }
        }
      }
    }
  }

  function isFutureDate(idate){
      var today = new Date().getTime(),
          idate = idate.split("/");

      idate = new Date(idate[2], idate[1] - 1, idate[0]).getTime();
      return (today - idate) < 0;
  }

  function btnCrearUsuario() {
    var d = $scope.crear_usuario;
    HomeService.crearUsuario(d)
    .success(function(res){
      if (res.err) {
        swal({
          title: "Hubo un error",
          text: "Ocurrio un error al tratar de crear el usuario",
          icon: "error"
        })
      } else {
        swal({
          title: "Todo bien",
          text: "Se creo el usuario en el sistema",
          icon: "success"
        }).then(function(){
          location.reload()
        })
      }
    })
  }

  function btnCambiarPass() {
    var d = $scope.cambiar_user;
    HomeService.cambiarPass(d)
    .success(function(res){
      if (res.err) {
        swal({
          icon: "error",
          title: "Tienes un error",
          text: "No se pudo cambiar la contraseña"
        })
      } else {
        swal({
          icon: "success",
          title: "Todo bien",
          text: "Se cambio la contraseña"
        }).then(function(){
          location.reload()
        })
      }
    })
  }

  function changeName() {
    var d = $scope.externo;
    HomeService.validarExterno(d)
    .success(function(res){
      if (res.err) {
        swal({
          icon: "error",
          title: "Tienes un error",
          text: "El participante que quieres agregar ya esta registrado en la base de datos"
        })
        $scope.listo = false
      } else {
        $scope.listo = true
      }
    })
  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
