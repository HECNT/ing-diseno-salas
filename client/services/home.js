// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('HomeService', ['$http', function($http) {
  var url = "https://localhost:3003";
  var urlBase = url + '/home';

  this.doLogin = function(d) {
    return $http.post( urlBase + '/login', d )
  }

  this.doKill = function() {
    return $http.put( urlBase + '/kill' )
  }

  this.getSala = function() {
    return $http.get( urlBase + '/get-sala' )
  }

  this.getHorario = function() {
    return $http.get( urlBase + '/get-horario' )
  }

  this.getParticipante = function() {
    return $http.get( urlBase + '/get-participante' )
  }

  this.setParticipante = function(d) {
    return $http.post( urlBase + '/set-participante', d )
  }

  this.getReport = function(d) {
    return $http.post( urlBase + '/get-report', d )
  }

  this.desactivarSala = function(d) {
    return $http.post( urlBase + '/desactivar-sala', d )
  }

  this.setNewSala = function(d) {
    return $http.post( urlBase + '/new-sala', d )
  }

  this.getAgenda = function() {
    return $http.get( urlBase + '/get-agenda')
  }

  this.getParticipanteAgenda = function(d) {
    return $http.post( urlBase + '/get-participante-agenda', d )
  }

  this.setNewAgenda = function(d) {
    return $http.post( urlBase + '/set-new-agenda', d )
  }

  this.getValidateHorario = function(d) {
    return $http.post( urlBase + '/get-validar-horario', d )
  }

  this.desactivarAgenda = function(d) {
    return $http.post( urlBase + '/desactivar-agenda', d )
  }
// TODO:
  this.validarDesactivar = function(d) {
    return $http.post( urlBase + '/validar-desactivar', d )
  }

  this.crearUsuario = function(d) {
    return $http.post( urlBase + '/crear-usuario', d )
  }

  this.cambiarPass = function(d) {
    return $http.post( urlBase + '/cambiar-pass', d )
  }

  this.validarExterno = function(d) {
    return $http.post( urlBase + '/validar-externo', d )
  }

}]);
